## Hotels and Reviews

This is the repository for Hotels And Reviews small project

### Project Design

Diagram to be added later

Design high overview

1- Console command to generate events to Kafka, with random Hotels and reviews

2- Kafka Consumer to consume the massages in Parallel and insert the data into the database

3- Console command to aggregate the data into stat tables

4- Api

5- Tests either unit test or integration tests based on the remaining time

6- Load test

### Links

Control Center http://localhost:9021/

steps :

- docker-compose up

- go to the control center and create topic : http://localhost:9021/clusters

- create topic called messages

- login into the fpm container

docker exec -it hotels-php-fpm /bin/bash

install composer

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

run composer install

- run bin console

there are 2 commands

# to generate data and send them to kafka

hotels:generate-data

# to consume the data from kafka

hotels:consume
