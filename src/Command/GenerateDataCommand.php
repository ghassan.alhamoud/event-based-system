<?php


namespace App\Hotels\Command;


use App\Hotels\Core\Command\BaseCommand;
use App\Hotels\Core\Interfaces\ProcessorInterface;
use Psr\Log\LoggerInterface;

class GenerateDataCommand extends BaseCommand {


  /**
   * GenerateDataCommand constructor.
   *
   * @param ProcessorInterface $cronProcessor
   * @param LoggerInterface    $logger
   */
  public function __construct(ProcessorInterface $cronProcessor, LoggerInterface $logger) {
    parent::__construct($cronProcessor, $logger);
  }

  protected function getCommandName(): string {
    return "hotels:generate-data";
  }

  protected function getCommandDescription(): string {
    return "This command is used to generated random data";
  }
}