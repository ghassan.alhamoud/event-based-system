<?php


namespace App\Hotels\DataGeneration\Config;


class GeneratorConfig {

  /**
   * @var int
   */
  private $numberOfHotels;

  /**
   * @var int
   */
  private $startDateInterval;

  /**
   * @var int
   */
  private $endDateInterval;

  /**
   * @var int
   */
  private $minReviewsByHotel;

  /**
   * @var int
   */
  private $maxReviewsPerHotel;

  /**
   * @var int
   */
  private $maxInfoLength;


  /**
   * RandomReviewConfig constructor.
   *
   * @param int      $numberOfHotels
   * @param int|null $startDate
   * @param int|null $endDate
   * @param int|null $maxReviewsPerHotel
   * @param int|null $minReviewsByHotel
   * @param int|null $maxInfoLength
   */
  public function __construct(int $numberOfHotels, ?int $startDate = null, ?int $endDate = null, ?int $maxReviewsPerHotel = null, ?int $minReviewsByHotel = null, ?int $maxInfoLength = 50) {
    $this->numberOfHotels  = $numberOfHotels;
    $this->startDateInterval  = $startDate;
    $this->endDateInterval    = $endDate;
    $this->maxReviewsPerHotel = $maxReviewsPerHotel;
    $this->minReviewsByHotel  = $minReviewsByHotel;
    $this->maxInfoLength  = $maxInfoLength;
  }

  /**
   * @return int
   */
  public function getStartDateInterval(): int {
    return $this->startDateInterval;
  }

  /**
   * @param int $startDateInterval
   */
  public function setStartDateInterval(int $startDateInterval): void {
    $this->startDateInterval = $startDateInterval;
  }

  /**
   * @return int
   */
  public function getEndDateInterval(): int {
    return $this->endDateInterval;
  }

  /**
   * @param int $endDateInterval
   */
  public function setEndDateInterval(int $endDateInterval): void {
    $this->endDateInterval = $endDateInterval;
  }

  /**
   * @return int
   */
  public function getMaxReviewsPerHotel(): int {
    return $this->maxReviewsPerHotel;
  }

  /**
   * @param int $maxReviewsPerHotel
   */
  public function setMaxReviewsPerHotel(int $maxReviewsPerHotel): void {
    $this->maxReviewsPerHotel = $maxReviewsPerHotel;
  }

  /**
   * @return int
   */
  public function getMinReviewsByHotel(): int {
    return $this->minReviewsByHotel;
  }

  /**
   * @param int $minReviewsByHotel
   */
  public function setMinReviewsByHotel(int $minReviewsByHotel): void {
    $this->minReviewsByHotel = $minReviewsByHotel;
  }

  /**
   * @return int
   */
  public function getMaxInfoLength(): int {
    return $this->maxInfoLength;
  }

  /**
   * @param int $maxInfoLength
   */
  public function setMaxInfoLength(int $maxInfoLength): void {
    $this->maxInfoLength = $maxInfoLength;
  }

  /**
   * @return int
   */
  public function getNumberOfHotels(): int {
    return $this->numberOfHotels;
  }

  /**
   * @param int $numberOfHotels
   */
  public function setNumberOfHotels(int $numberOfHotels): void {
    $this->numberOfHotels = $numberOfHotels;
  }
}
