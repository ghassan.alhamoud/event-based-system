<?php


namespace App\Hotels\DataGeneration\Config;


class RandomHotelConfig {

  /**
   * @var int
   */
  private $maxNameLength;

  /**
   * @var int
   */
  private $minReviewsByHotel;

  /**
   * @var int
   */
  private $maxReviewsPerHotel;

  /**
   * RandomHotelConfig constructor.
   *
   * @param int $maxNameLength
   * @param int $minReviewsByHotel
   * @param int $maxReviewsPerHotel
   */
  public function __construct(int $maxNameLength, int $minReviewsByHotel, int $maxReviewsPerHotel) {
    $this->maxNameLength     = $maxNameLength;
    $this->minReviewsByHotel = $minReviewsByHotel;
    $this->maxReviewsPerHotel     = $maxReviewsPerHotel;
  }

  /**
   * @return int
   */
  public function getMaxNameLength(): int {
    return $this->maxNameLength;
  }

  /**
   * @return int
   */
  public function getMinReviewsByHotel(): int {
    return $this->minReviewsByHotel;
  }

  /**
   * @return int
   */
  public function getMaxReviewsPerHotel(): int {
    return $this->maxReviewsPerHotel;
  }
}
