<?php


namespace App\Hotels\DataGeneration\Interfaces;

use App\Hotels\Core\Dto\ReviewDto;

interface RandomReviewGeneratorInterface {

  public function getReview(string $hotelId) : ReviewDto;
}