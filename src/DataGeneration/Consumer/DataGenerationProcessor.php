<?php


namespace App\Hotels\DataGeneration\Consumer;


use App\Hotels\Core\Dto\HotelDto;
use App\Hotels\Core\Interfaces\KafkaProcessorInterface;
use Symfony\Component\Serializer\SerializerInterface;

class DataGenerationProcessor implements KafkaProcessorInterface {

  /**
   * @var SerializerInterface
   */
  private $serializer;

  /**
   * DataGenerationProcessor constructor.
   *
   * @param SerializerInterface $serializer
   */
  public function __construct(SerializerInterface $serializer) {
    $this->serializer = $serializer;
  }

  public function process(object $message): void {
    if (empty($message->payload) || ($message->err ?? 0) < 0) {
      return;
    }
    try {
      /** @var HotelDto $payload */
      $payload = $this->serializer->deserialize(($message->payload), HotelDto::class, 'json');;
      print_r($payload->getId() . PHP_EOL);
    } catch (\Exception $exception) {
      echo($exception->getMessage());
      echo ($message->payload);
    }
  }
}