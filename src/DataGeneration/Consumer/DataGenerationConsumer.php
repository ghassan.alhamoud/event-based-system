<?php


namespace App\Hotels\DataGeneration\Consumer;


use App\Hotels\Core\Command\BaseConsumer;

class DataGenerationConsumer extends BaseConsumer {

  protected function getCommandName(): string {
    return "hotels:consume";
  }

  protected function getCommandDescription(): string {
    return "Data Generator Kafka Consumer";
  }
}
