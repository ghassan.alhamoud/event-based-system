<?php


namespace App\Hotels\DataGeneration\Service;


use App\Hotels\Core\Dto\ReviewDto;
use App\Hotels\DataGeneration\Config\RandomReviewConfig;
use App\Hotels\DataGeneration\Interfaces\RandomReviewGeneratorInterface;
use App\Hotels\Core\Interfaces\RandomDateGeneratorInterface;
use App\Hotels\Core\Interfaces\RandomIntGeneratorInterface;
use App\Hotels\Core\Interfaces\RandomStringGeneratorInterface;

class RandomReviewGenerator implements RandomReviewGeneratorInterface {

  /**
   * @var RandomIntGeneratorInterface
   */
  private $randomIntGenerator;

  /**
   * @var RandomStringGeneratorInterface
   */
  private $randomStringGenerator;

  /**
   * @var RandomDateGeneratorInterface
   */
  private $randomDateGenerator;

  /**
   * @var RandomReviewConfig
   */
  private $config;

  /**
   * RandomReviewGenerator constructor.
   *
   * @param RandomIntGeneratorInterface    $randomIntGenerator
   * @param RandomStringGeneratorInterface $randomStringGenerator
   * @param RandomDateGeneratorInterface   $randomDateGenerator
   * @param RandomReviewConfig             $config
   */
  public function __construct(
      RandomIntGeneratorInterface $randomIntGenerator, RandomStringGeneratorInterface $randomStringGenerator, RandomDateGeneratorInterface $randomDateGenerator, RandomReviewConfig $config
  ) {
    $this->randomIntGenerator    = $randomIntGenerator;
    $this->randomStringGenerator = $randomStringGenerator;
    $this->randomDateGenerator     = $randomDateGenerator;
    $this->config              = $config;
  }

  public function getReview(string $hotelId): ReviewDto {

    $id = null;
    $rating = $this->randomIntGenerator->get(0, 5);
    $info = $this->randomStringGenerator->get(1, $this->config->getMaxInfoLength());
    $reviewDate = $this->randomDateGenerator->get($this->config->getStartDateInterval(), $this->config->getEndDateInterval());

    return new ReviewDto($id, $rating, $info, $reviewDate, $hotelId);
  }
}