<?php


namespace App\Hotels\DataGeneration\Service;


use App\Hotels\Core\Dto\HotelDto;
use App\Hotels\DataGeneration\Config\RandomHotelConfig;
use App\Hotels\DataGeneration\Interfaces\RandomHotelGeneratorInterface;
use App\Hotels\DataGeneration\Interfaces\RandomReviewGeneratorInterface;
use App\Hotels\Core\Interfaces\RandomDateGeneratorInterface;
use App\Hotels\Core\Interfaces\RandomIntGeneratorInterface;
use App\Hotels\Core\Interfaces\RandomStringGeneratorInterface;

class RandomHotelGenerator implements RandomHotelGeneratorInterface {

  /**
   * @var RandomIntGeneratorInterface
   */
  private $randomIntGenerator;

  /**
   * @var RandomStringGeneratorInterface
   */
  private $randomStringGenerator;

  /**
   * @var RandomDateGeneratorInterface
   */
  private $randomDateGenerator;

  /**
   * @var RandomReviewGeneratorInterface
   */
  private $randomReviewGenerator;

  /**
   * @var RandomHotelConfig
   */
  private $config;

  /**
   * RandomHotelGenerator constructor.
   *
   * @param RandomIntGeneratorInterface    $randomIntGenerator
   * @param RandomStringGeneratorInterface $randomStringGenerator
   * @param RandomDateGeneratorInterface   $randomDateGenerator
   * @param RandomReviewGeneratorInterface $randomReviewGenerator
   * @param RandomHotelConfig              $config
   */
  public function __construct(
      RandomIntGeneratorInterface $randomIntGenerator, RandomStringGeneratorInterface $randomStringGenerator, RandomDateGeneratorInterface $randomDateGenerator,
      RandomReviewGeneratorInterface $randomReviewGenerator, RandomHotelConfig $config
  ) {
    $this->randomIntGenerator    = $randomIntGenerator;
    $this->randomStringGenerator = $randomStringGenerator;
    $this->randomDateGenerator     = $randomDateGenerator;
    $this->randomReviewGenerator     = $randomReviewGenerator;
    $this->config              = $config;
  }

  public function getHotel(): HotelDto {
    $id = null;
    $name = $this->randomStringGenerator->get(1, $this->config->getMaxNameLength());
    $reviews = [];
    $numOfReviewsToGenerate = $this->randomIntGenerator->get($this->config->getMinReviewsByHotel(), $this->config->getMaxReviewsPerHotel());
    $hotel = new HotelDto($id, $name, $reviews);
//    while ($numOfReviewsToGenerate-- > 0) {
//      $review = $this->randomReviewGenerator->getReview($hotel->getId());
//      $hotel->addReview($review);
//    }

    return $hotel;
  }
}
