<?php


namespace App\Hotels\DataGeneration\Processor;


use App\Hotels\Core\Dto\HotelDto;
use App\Hotels\Core\Event\HotelEvent;
use App\Hotels\Core\Interfaces\EventEmitterInterface;
use App\Hotels\Core\Interfaces\ProcessorInterface;
use App\Hotels\DataGeneration\Config\GeneratorConfig;
use App\Hotels\DataGeneration\Interfaces\RandomHotelGeneratorInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateDataCommandProcessor implements ProcessorInterface {

  /**
   * @var RandomHotelGeneratorInterface
   */
  private $randomHotelGenerator;

  /**
   * @var GeneratorConfig
   */
  private $config;

  /**
   * @var EventEmitterInterface
   */
  private $eventEmitter;

  /**
   * GenerateDataCommandProcessor constructor.
   *
   * @param RandomHotelGeneratorInterface $generator
   * @param GeneratorConfig               $config
   * @param EventEmitterInterface         $eventEmitter
   */
  public function __construct(RandomHotelGeneratorInterface $generator, GeneratorConfig $config, EventEmitterInterface $eventEmitter) {
    $this->randomHotelGenerator = $generator;
    $this->config = $config;
    $this->eventEmitter = $eventEmitter;
  }

  public function process(InputInterface $input, OutputInterface $output) : void {
    $numOfHotels = $this->config->getNumberOfHotels();
    $hotels = [];
    while ($numOfHotels-- > 0) {
      $hotel = $this->randomHotelGenerator->getHotel();
      $this->publish($hotel);
      $hotels[] = $hotel;
    }
//    var_dump($hotels);
  }

  private function publish(HotelDto $hotel) : void {
    $event = new HotelEvent($hotel);
    $this->eventEmitter->emit($event);
  }
}