<?php


namespace App\Hotels\Core\Dto;


use JsonSerializable;
use Ramsey\Uuid\Uuid;

class HotelDto implements JsonSerializable {
  /**
   * @var string
   */
  private $id;

  /**
   * @var string
   */
  private $name;

  /**
   * @var ReviewDto[]
   */
  private $reviews;

  /**
   * HotelDto constructor.
   *
   * @param string|null $id
   * @param string      $name
   * @param ReviewDto[] $reviews
   */
  public function __construct(?string $id , string $name, array $reviews = []) {
    $this->id      = $id ?? Uuid::uuid4();
    $this->name    = $name;
    $this->reviews = $reviews;
  }

  /**
   * @return string
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * @param string $id
   */
  public function setId(string $id): void {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName(string $name): void {
    $this->name = $name;
  }

  /**
   * @return ReviewDto[]
   */
  public function getReviews(): array {
    return $this->reviews;
  }

  /**
   * @param ReviewDto[] $reviews
   */
  public function setReviews(array $reviews): void {
    $this->reviews = $reviews;
  }

  /**
   * @param ReviewDto $review
   */
  public function addReview(ReviewDto $review): void {
    $review->setHotelId($this->getId());
    $this->reviews[] = $review;
  }

  public function jsonSerialize() {
    return get_object_vars($this);
  }
}
