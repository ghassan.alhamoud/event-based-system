<?php


namespace App\Hotels\Core\Dto;


use DateTimeImmutable;
use JsonSerializable;
use Ramsey\Uuid\Uuid;

class ReviewDto implements JsonSerializable {
  /**
   * @var string
   */
  private $id;

  /**
   * @var string
   */
  private $hotelId;

  /**
   * @var int
   */
  private $rating;

  /**
   * @var string
   */
  private $info;

  /**
   * @var DateTimeImmutable
   */
  private $reviewDate;

  /**
   * ReviewEntity constructor.
   *
   * @param string|null       $id
   * @param int               $rating
   * @param string            $info
   * @param DateTimeImmutable $reviewDate
   * @param string            $hotelId
   */
  public function __construct(?string $id, int $rating, string $info, DateTimeImmutable $reviewDate, string $hotelId = '') {
    $this->id      = $id ?? Uuid::uuid4();
    $this->rating  = $rating;
    $this->info    = $info;
    $this->reviewDate    = $reviewDate;
    $this->hotelId = $hotelId;
  }

  /**
   * @return string
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * @param string $id
   */
  public function setId(string $id): void {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getHotelId(): string {
    return $this->hotelId;
  }

  /**
   * @param string $hotelId
   */
  public function setHotelId(string $hotelId): void {
    $this->hotelId = $hotelId;
  }

  /**
   * @return int
   */
  public function getRating(): int {
    return $this->rating;
  }

  /**
   * @param int $rating
   */
  public function setRating(int $rating): void {
    $this->rating = $rating;
  }

  /**
   * @return string
   */
  public function getInfo(): string {
    return $this->info;
  }

  /**
   * @param string $info
   */
  public function setInfo(string $info): void {
    $this->info = $info;
  }

  /**
   * @return DateTimeImmutable
   */
  public function getReviewDate(): DateTimeImmutable {
    return $this->reviewDate;
  }

  public function jsonSerialize() {
    return get_object_vars($this);
  }
}
