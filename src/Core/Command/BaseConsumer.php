<?php

namespace App\Hotels\Core\Command;


use App\Hotels\Core\Factory\ConsumerFactory;
use App\Hotels\Core\Interfaces\KafkaProcessorInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class BaseConsumer extends Command {

  /**
   * @var ConsumerFactory
   */
  private $factory;

  /**
   * @var KafkaProcessorInterface
   */
  private $processor;

  /**
   * @var string
   */
  private $consumerGroup;

  /**
   * @var string
   */
  private $brokerList;

  /**
   * @var string
   */
  private $topic;

  /**
   * @var LoggerInterface
   */
  private $logger;

  /**
   * BaseCommand constructor.
   *
   * @param LoggerInterface         $logger
   * @param ConsumerFactory         $factory
   * @param KafkaProcessorInterface $processor
   * @param string                  $consumerGroup
   * @param string                  $brokerList
   * @param string                  $topic
   */
  public function __construct(LoggerInterface $logger, ConsumerFactory $factory, KafkaProcessorInterface $processor, string $consumerGroup, string $brokerList, string $topic) {
    $this->logger = $logger;
    $this->factory = $factory;
    $this->processor = $processor;
    $this->consumerGroup = $consumerGroup;
    $this->brokerList = $brokerList;
    $this->topic = $topic;
    parent::__construct();
  }

  /**
   * Configures the current command.
   *
   * @return void
   */
  protected function configure(): void {
    $this->setDescription($this->getCommandDescription());
    $this->setName($this->getCommandName());
  }

  /**
   * @return string
   */
  abstract protected function getCommandName(): string;

  /**
   * @return string
   */
  abstract protected function getCommandDescription(): string;

  /**
   * @param InputInterface  $input  .
   * @param OutputInterface $output .
   *
   * @return int|null|void
   */
  protected function execute(InputInterface $input, OutputInterface $output) : int {
    try {
      $consumer = $this->factory->getConsumer($this->consumerGroup, $this->brokerList, $this->processor);
      $consumer->subscribe($this->topic);
      $consumer->consume();
      return 1;
    } catch (\Throwable $exception) {
      $this->logger->error(
          $exception->getMessage(),
          [
              'stack_message' => $exception->getTraceAsString(),
              'command' => $input->getFirstArgument(),
          ]
      );
      return 0;
    }
  }
}