<?php

namespace App\Hotels\Core\Command;


use App\Hotels\Core\Interfaces\ProcessorInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class BaseCommand extends Command {

  /**
   * @var LoggerInterface
   */
  private $logger;
  /**
   * @var ProcessorInterface
   */
  private $cronProcessor;

  /**
   * BaseCommand constructor.
   *
   * @param ProcessorInterface $cronProcessor .
   * @param LoggerInterface    $logger        .
   */
  public function __construct(ProcessorInterface $cronProcessor, LoggerInterface $logger) {
    $this->cronProcessor = $cronProcessor;
    $this->logger = $logger;
    parent::__construct();
  }

  /**
   * Configures the current command.
   *
   * @return void
   */
  protected function configure(): void {
    $this->setDescription($this->getCommandDescription());
    $this->setName($this->getCommandName());
  }

  /**
   * @return string
   */
  abstract protected function getCommandName(): string;

  /**
   * @return string
   */
  abstract protected function getCommandDescription(): string;

  /**
   * @param InputInterface  $input  .
   * @param OutputInterface $output .
   *
   * @return int|null|void
   */
  protected function execute(InputInterface $input, OutputInterface $output) : int {
    try {
      $this->cronProcessor->process($input, $output);
      return 1;
    } catch (\Throwable $exception) {
      $this->logger->error(
          $exception->getMessage(),
          [
              'stack_message' => $exception->getTraceAsString(),
              'command' => $input->getFirstArgument(),
          ]
      );
      return 0;
    }
  }
}