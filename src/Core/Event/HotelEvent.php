<?php


namespace App\Hotels\Core\Event;


use App\Hotels\Core\Dto\HotelDto;
use App\Hotels\Core\Interfaces\EventInterface;

class HotelEvent implements EventInterface {

  /**
   * @var HotelDto
   */
  private $payload;

  /**
   * HotelEvent constructor.
   *
   * @param HotelDto $payload
   */
  public function __construct(HotelDto $payload) {
    $this->payload = $payload;
  }

  public function getTopicName(): string {
    return 'messages';
  }

  public function getKey(): string {
    return $this->payload->getId();
  }

  public function getPayload(): object {
    return $this->payload;
  }
}