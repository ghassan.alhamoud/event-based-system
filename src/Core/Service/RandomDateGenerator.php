<?php


namespace App\Hotels\Core\Service;


use App\Hotels\Core\Interfaces\RandomDateGeneratorInterface;
use DateTime;
use DateTimeImmutable;

class RandomDateGenerator implements RandomDateGeneratorInterface{

  public function get(DateTimeImmutable $from, DateTimeImmutable $to): DateTimeImmutable {
    try {
      $randomTimestamp = random_int($from->getTimestamp(), $to->getTimestamp());
      $randomDate = new DateTimeImmutable();
      $randomDate->setTimestamp($randomTimestamp);

      return $randomDate;
    } catch (\Exception $e) {
      var_dump($e);
    }
    return DateTimeImmutable::createFromFormat(DateTimeImmutable::ATOM, new DateTime());
  }
}
