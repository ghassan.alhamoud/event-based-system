<?php


namespace App\Hotels\Core\Service;


use App\Hotels\Core\Interfaces\DateTimeHelperInterface;
use DateInterval;
use DateTimeImmutable;

class DateTimeHelper implements DateTimeHelperInterface {

  public function getDate(int $interval): DateTimeImmutable {
    $positiveInt = abs($interval);
    $dateInterval = new DateInterval("P$positiveInt"."D");
    if ($interval < 0) {
      $dateInterval->invert = 1;
    }

    return (new DateTimeImmutable('now'))->add($dateInterval);
  }
}
