<?php


namespace App\Hotels\Core\Service;


use App\Hotels\Core\Interfaces\EventEmitterInterface;
use App\Hotels\Core\Interfaces\EventInterface;
use RdKafka\Conf;
use RdKafka\Producer;

class EventEmitter implements EventEmitterInterface {

  public function emit(EventInterface $event): void {
    $conf = new Conf();
    $conf->set('metadata.broker.list', 'broker:29092');
    $producer = new Producer($conf);

    $topic = $producer->newTopic($event->getTopicName());
    $topic->produce(RD_KAFKA_PARTITION_UA, 0, json_encode($event->getPayload()), $event->getKey());
    $producer->poll(0);

    for ($flushRetries = 0; $flushRetries < 10; $flushRetries++) {
      $result = $producer->flush(10000);
      if (RD_KAFKA_RESP_ERR_NO_ERROR === $result) {
        break;
      }
    }

    if (RD_KAFKA_RESP_ERR_NO_ERROR !== $result) {
      throw new \RuntimeException('Was unable to flush, messages might be lost!');
    }
  }
}