<?php


namespace App\Hotels\Core\Factory;


use App\Hotels\Consumer\ConsumerConfig;
use App\Hotels\Consumer\Consumer;
use App\Hotels\Core\Interfaces\ConsumerFactoryInterface;
use App\Hotels\Core\Interfaces\ConsumerInterface;
use App\Hotels\Core\Interfaces\KafkaProcessorInterface;

class ConsumerFactory implements ConsumerFactoryInterface {

  public function getConsumer(string $consumerGroup, string $brokerList, KafkaProcessorInterface $processor): ConsumerInterface {
    $config = new ConsumerConfig($consumerGroup, $brokerList);
    return new Consumer($config, $processor);
  }
}