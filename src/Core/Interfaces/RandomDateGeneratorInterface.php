<?php


namespace App\Hotels\Core\Interfaces;


use DateTimeImmutable;

interface RandomDateGeneratorInterface {

  /**
   * @param DateTimeImmutable $from
   * @param DateTimeImmutable $to
   *
   * @return DateTimeImmutable
   */
  public function get(DateTimeImmutable $from, DateTimeImmutable $to) : DateTimeImmutable;
}
