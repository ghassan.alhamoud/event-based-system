<?php


namespace App\Hotels\Core\Interfaces;


use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

interface ProcessorInterface {

  public function process(InputInterface $input, OutputInterface $output);
}