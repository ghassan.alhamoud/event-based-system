<?php


namespace App\Hotels\Core\Interfaces;

interface ConsumerInterface {

  public function subscribe(string $topicName): void;

  public function consume() ;
}
