<?php


namespace App\Hotels\Core\Interfaces;


use DateTimeImmutable;

interface DateTimeHelperInterface {
  /**
   * @param int $interval
   *
   * @return DateTimeImmutable
   */
  public function getDate(int $interval) : DateTimeImmutable;
}
