<?php


namespace App\Hotels\Core\Interfaces;

interface ConsumerFactoryInterface {

  public function getConsumer(string $consumerGroup, string $brokerList, KafkaProcessorInterface $processor): ConsumerInterface ;
}
