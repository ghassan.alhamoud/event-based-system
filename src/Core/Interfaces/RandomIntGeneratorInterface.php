<?php


namespace App\Hotels\Core\Interfaces;


interface RandomIntGeneratorInterface {

  /**
   * @param int $min
   * @param int $max
   *
   * @return mixed
   */
  public function get(int $min, int $max) : int;
}