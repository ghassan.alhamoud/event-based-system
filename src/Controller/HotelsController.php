<?php

namespace App\Hotels\Controller;

use RdKafka\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use RdKafka\Conf;
use RdKafka\Producer;
use RdKafka\KafkaConsumer;
use Throwable;

class HotelsController extends AbstractController {

  /**
   * @return Response
   * @throws \Exception
   */
  public function hotels(): Response {
    $number = random_int(0, 100);

    $conf = new Conf();
    $conf->set('metadata.broker.list', 'broker:29092');

    //If you need to produce exactly once and want to keep the original produce order, uncomment the line below
    //$conf->set('enable.idempotence', 'true');

    $producer = new Producer($conf);

    $topic = $producer->newTopic("test");

    for ($i = 0; $i < 1000; $i++) {
      $topic->produce(RD_KAFKA_PARTITION_UA, 0, json_encode(["Message $i" => $i]));
      $producer->poll(0);
    }

    for ($flushRetries = 0; $flushRetries < 10; $flushRetries++) {
      $result = $producer->flush(10000);
      if (RD_KAFKA_RESP_ERR_NO_ERROR === $result) {
        break;
      }
    }

    if (RD_KAFKA_RESP_ERR_NO_ERROR !== $result) {
      throw new \RuntimeException('Was unable to flush, messages might be lost!');
    }

    return new Response(
        '<html><body>Lucky number: '.$number.'</body></html>'
    );
  }

  public function consume() {
    $conf = new Conf();

    // Configure the group.id. All consumer with the same group.id will consume
    // different partitions.
    $conf->set('group.id', 'groups');

    // Initial list of Kafka brokers
    $conf->set('metadata.broker.list', 'broker:29092');

    // Set where to start consuming messages when there is no initial offset in
    // offset store or the desired offset is out of range.
    // 'smallest': start from the beginning
    $conf->set('auto.offset.reset', 'smallest');
    $consumer = new KafkaConsumer($conf);

    $messages = [];

    try {
      $consumer->subscribe(['test']);

      $message = 'subscribed to topic ' . 'test';
      $messages[] = $message;
      $messages[] = $consumer->getSubscription();
      $messages[] = $consumer->getMetadata(true, null, 500);
    } catch (Throwable $readException) {
      $messages[] = 'unable to subscribe to topic test' ;
      $messages[] = $readException->getMessage();
    }


    $i = 0;

    while ($i++ < 10) {

      try {
        $message = $consumer->consume(.5 * 1000);
        $messages[] = $message->payload . ' --> ' . $message->topic_name . ' --> ' . $message->offset;

      } catch (Exception $e) {
        $messages[] = $e->getMessage();
      }
    }
    return new JsonResponse($messages);
  }
}
