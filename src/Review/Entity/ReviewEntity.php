<?php


namespace App\Hotels\Review\Entity;


class ReviewEntity {

  /**
   * @var string
   */
  private $id;

  /**
   * @var string
   */
  private $hotelId;

  /**
   * @var int
   */
  private $rating;

  /**
   * @var string
   */
  private $info;

  /**
   * ReviewEntity constructor.
   *
   * @param string $id
   * @param string $hotelId
   * @param int    $rating
   * @param string $info
   */
  public function __construct(string $id, string $hotelId, int $rating, string $info) {
    $this->id      = $id;
    $this->hotelId = $hotelId;
    $this->rating  = $rating;
    $this->info    = $info;
  }

  /**
   * @return string
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * @param string $id
   */
  public function setId(string $id): void {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getHotelId(): string {
    return $this->hotelId;
  }

  /**
   * @param string $hotelId
   */
  public function setHotelId(string $hotelId): void {
    $this->hotelId = $hotelId;
  }

  /**
   * @return int
   */
  public function getRating(): int {
    return $this->rating;
  }

  /**
   * @param int $rating
   */
  public function setRating(int $rating): void {
    $this->rating = $rating;
  }

  /**
   * @return string
   */
  public function getInfo(): string {
    return $this->info;
  }

  /**
   * @param string $info
   */
  public function setInfo(string $info): void {
    $this->info = $info;
  }
}
