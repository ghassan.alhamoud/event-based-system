<?php


namespace App\Hotels\Consumer;


class ConsumerConfig {

  /**
   * @var string
   */
  private $brokerList;

  /**
   * @var string
   */
  private $consumerGroup;

  /**
   * ConsumerConfig constructor.
   *
   * @param string $brokerList
   * @param string $consumerGroup
   */
  public function __construct(string $brokerList, string $consumerGroup) {
    $this->brokerList    = $brokerList;
    $this->consumerGroup = $consumerGroup;
  }

  /**
   * @return string
   */
  public function getBrokerList(): string {
    return $this->brokerList;
  }

  /**
   * @return string
   */
  public function getConsumerGroup(): string {
    return $this->consumerGroup;
  }
}